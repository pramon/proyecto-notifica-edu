<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas debe contener al menos 6 caracteres y deben coincidir',
    'reset' => 'Su password ha sido cambiado',
    'sent' => 'Se ha enviado un enlace a su correo que le permitirá restaurar la contraseña',
    'token' => 'El token de validación es erróneo, revise su correo y solo acceda al enlace más reciente',
    'user' => "El email introducido no está en la base de datos",

];
