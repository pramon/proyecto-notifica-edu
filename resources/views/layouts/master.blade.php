<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Notificaciones</title>

        <!-- Bootstrap -->
        <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <style>
            .bordeNavegacion{
                border: 2px solid #000100;
            }
            .sepBotonesNav{
                border: 2px solid #FFFFFF;
            }
            .textoPie{
                font-size:smaller;
                font-weight: bold


            }
            .infoPie{
                font-size:smaller;
                font-weight: bold;
                text-align: right;
            }
            body {
                font-family: 'Lato';
                padding-bottom: 10px
            }
            .cabInicio{
                padding-top: 30px;


            }
            .titulo{
                text-align: center;
                padding-top: 20px;
                font-size: xx-large;
            }

            .rolCabecera{
                text-align: center;
                font-size: small;
                vertical-align: middle;

            }
            .colorOpcion{
                color: #000000;
            }
            thead{
                background-color: #EEE;
                border: #000000 solid 2px;
            }
            .radioInput{

                vertical-align: top;
            }
            
            .bloqueOculto {
                display: none;
            }
            .textoBienvenida{
                font-size: 16px;
                text-align: justify;
            }
            /*para el popover de información*/
            .popover-title {

                font-size: 12px;
                background-color: #5bc0de
            }
            .popover-content {

                font-size: 10px;
            }
            /*Para las noticias en detalle*/
            .tablaNoticia{
                width: 100%;
            }
            .cuerpoNoticia{
                text-align: justify;
            }

            .fotoNoticia img{
                display: block;
                margin-left: auto;
                margin-right: auto;
            }

            textarea {
                width: 100%;
                height: 150px;
            }
            tr th{
                text-align: center;
            }
            /*Para las noticias en lista*/

            .tnl{
                font-size: large;
                font-weight: bolder;
                text-align: center;
                color: #5bc0de;
            }
            .table tbody>tr>td.vert-align-foto{
                vertical-align: middle;
                height:120px;
                width:120px;
                text-align: center;
                min-height: 120px;
                max-height: 120px;
                min-height: 120px;
                max-height:120px;
            }


            /*Para la busqueda de la tabla*/

            
            .no-result{
                display:none;
            }
            .results tbody tr[visible='false']{
                display:none;
               
            }
            .results tbody tr[visible='true']{
                display:table-row;
              
            }

            .counter{
                padding:8px; 
                color:#ccc;
            }
            

            .colorClaroWarning{
                background-color: #F79F81;
            }


        </style>
        <!--jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed-->
        <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript">
$(document).ready(function () {
    //para el boton de información

    $('[data-toggle="popover"]').popover({
        placement: 'down'
    });
});

$(document).ready(function () {
    $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {

            $(this).attr('visible', 'false');

        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
       // var jobCount = $('.results tbody tr[visible="false"]').length;
        $('.counter').text(jobCount + ' item');

        if (jobCount == '0') {
            $('.no-result').show();
        } else {
            $('.no-result').hide();
        }
    });
});

//Para ocultar un bloque 
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}
function mostrarBloque(id) {
    var e = document.getElementById(id);
    e.style.display = 'block';
}
//Para que se pueda hacer click sobre una fila completa
jQuery(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
});
        </script>
    </head>
    <body>
        <div class="container">
            <div>
                @include('partials.header')
            </div>
            <div class="row">
                <div class="col-md-2">
                    @include('partials.navbar')
                </div>

                <div class="col-md-10 main ">


                    @yield('content')

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="container navbar navbar-fixed-bottom hidden-xs" >
                @include('partials.footer')
            </div>
        </div>


    </body>
</html>