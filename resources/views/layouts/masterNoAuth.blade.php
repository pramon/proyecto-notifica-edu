<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Notificaciones</title>

        <!-- Bootstrap -->
        <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <style>
            .bordeNavegacion{
                border: 2px solid #000100;
            }
            .sepBotonesNav{
                border: 2px solid #FFFFFF;
            }
            .textoPie{
                font-size:smaller;
                font-weight: bold


            }
            .infoPie{
                font-size:smaller;
                font-weight: bold;
                text-align: right;
            }
            body {
                font-family: 'Lato';
                padding-bottom: 10px
            }
            .titulo{
                text-align: center;
                font-size: xx-large;

            }
            .rolCabecera{
                text-align: center;
                font-size: small;
                vertical-align: middle;

            }

        </style>
    </head>
    <body>
        <div class="container">
            <div>
                <header class="navbar navbar-default">
                    <div class="row">
                        <div class="col-md-2">

                            <img src="{{ url('/assets/imagenes/logo.jpg') }}" class="img-responsive" width="175" height="175"/>

                        </div>
                        <div class="col-md-8">
                            <div class="titulo">Notificaciones</div>
                            <a href="{{ url('/register') }}">Registrarse</a>
                           

                        </div>
                        <div class="col-md-2 rolCabecera ">
                            <br>
                            

                        </div>
                    </div>   


                </header>
            </div>
            <div class="row">
                

                <div class="col-md-12 main ">


                    @yield('content')

                </div>
            </div>
            <div class="container navbar navbar-fixed-bottom hidden-xs" >
                @include('partials.footer')
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    </body>
</html>