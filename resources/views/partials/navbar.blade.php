<nav class="navbar navbar-default bordeNavegacion">
    <!--  <div class="container">-->
    <!-- Solo superadmin-->
    @if(Auth::user()->rol<2)
    <a href="{{url('/superadmin/crear/administrador')}}">
        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
        Crear Administrador
    </a>
    <!--FIN solo superadmin-->
    @endif
    
    <!--Solo administrador-->
     @if(Auth::user()->rol<3)
    <div class="botonMenu" style="background-color: #000; color:#ffffff; text-align: center " >Módulo de administrador </div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionGestDatos">Gestionar Datos</button>

    <div id="accordionGestDatos" class="collapse">

        <a href="{{url('/admin/datos/importar')}}">
            <span class="colorOpcion">   <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                Importar  Datos </span>
        </a>

        <br>

        <a href="{{url('/admin/datos/exportar')}}">
            <span class="colorOpcion"> <span class="colorOpcion">  <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>
                    Copia de Seguridad</span>
        </a>

        <br>
        <a href="{{url('/admin/datos/restaurar')}}">
            <span class="colorOpcion">  <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
                Restaurar Copia de Seguridad </span>
        </a>
        <br>
        <a href="{{url('/admin/datos/generar')}}">
            <span class="colorOpcion">  <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                Generar y enviar claves a familias </span>
        </a>
        <br>
    </div>
    <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionNoticias">Noticias</button>
    <div id="accordionNoticias" class="collapse">
        
       
        
        
        <a href="{{url('/admin/insertar/noticia')}}">
            <span class="colorOpcion">  <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                Nueva </span>
        </a>
        <br>
        
        <a href="{{url('/admin/consultar/todasNoticias')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                Todas las noticias </span>
        </a>
        <br>
        <a href="{{url('/admin/consultar/noticias')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                Noticias Publicadas </span>
        </a>
        <br>
        
    </div>

    <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionNotificaciones">Notificaciones</button>
    <div id="accordionNotificaciones" class="collapse">
        
        
        <a href="{{url('/admin/insertar/notificacionAlumno')}}">
            <span class="colorOpcion">  <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                Nueva </span>
        </a>
        <br>
        <a href="{{url('/admin/insertar/notificacionGlobal')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                Nueva Global </span>
        </a>
        <br>
        <a href="{{url('/admin/consultar/notificaciones')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                Ver Notificaciones </span>
        </a>
        <br>
        
    </div>

    <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionAlumnos">Alumnos</button>
    <div id="accordionAlumnos" class="collapse">
        <a href="{{url('/admin/insertar/alumno')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-education" aria-hidden="true"></span>
                Nuevo </span>
        </a>
        <br>
        <a href="{{url('/admin/consultar/alumnos')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-education" aria-hidden="true"></span>
                Ver Todos </span>
        </a>
        <br>
       <!-- <a href="{{url('/admin/asignar/famAlu')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
                Famila a alumno </span>
        </a>
        <br>
        <a href="{{url('/admin/asignar/grupAlu')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                Grupo a alumno </span>
        </a>
        <br>
        <a href="{{url('/admin/asignar/rolAdmin')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-king" aria-hidden="true"></span>
                Rol de administrador </span>
        </a>
        <br>-->
    </div>
    <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionFamilias">Familias</button>
    <div id="accordionFamilias" class="collapse">
        <a href="{{url('/admin/insertar/familia')}}">
            <span class="colorOpcion">  <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
                Nueva </span>
        </a>
        <br>
        <a href="{{url('/admin/consultar/familias')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
                Ver </span>
        </a>
        <br>
        
    </div>
     <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionConsultas">Consultar</button>
    <div id="accordionConsultas" class="collapse">
         <a href="{{url('/admin/consultar/usuarios')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                Profesores </span>
        </a>
        <br>
        <a href="{{url('/admin/consultar/grupos')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                Grupos </span>
        </a>
        <br>
        
    </div>
    @endif
    <!--Modulo del profesor-->



    
    
    <div class="botonMenu" style="background-color: #000; color:#ffffff; text-align: center " >Módulo del profesor </div>
    <div class="botonMenu"></div>
    <button type="button" class="btn btn-info btn-block sepBotonesNav" data-toggle="collapse" data-target="#accordionFaltas">Faltas</button>
    <div id="accordionFaltas" class="collapse">
        <a href="{{url('/admin/asignar/famAlu')}}">
            <span class="colorOpcion"> <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
                Grupo </span>
        </a>
        
    </div>



    <!--Fin Modulo de profesor-->

    
</nav>
