<header class="navbar navbar-default">
    <div class="row">
        <div class="col-md-2">

            <img src="{{ url('/assets/imagenes/logo.jpg') }}" class="img-responsive" width="175" height="175"/>

        </div>
        <div class="col-md-8">

            <div class="row"><!--fila de inicio y titulo-->
                <div class=" col-md-3">
                    
                    <a href="{{url('/admin/bienvenida')}}">
                        <span class="glyphicon glyphicon-home cabInicio" aria-hidden="true"></span>
                        Inicio
                    </a>
                </div>

                <div class="col-md-9 titulo">Notificaciones</div>
            </div><!--fin row de inic y titulo-->



        </div>
        <div class="col-md-2 rolCabecera ">
            <br>
            <p> <a href="{{url('/perfil')}}">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</a> como<br>
                @if(Auth::user()->rol==1)Superadministrador @endif @if(Auth::user()->rol==2)Adminstrador @endif @if(Auth::user()->rol==3)Profesor @endif</p>
            <a href="{{url('/logout')}}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                Cerrar sesión
            </a>

        </div>
    </div>   


</header>

