@extends('layouts.master')

@section('content')




<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title text-center">
            <span  aria-hidden="true"></span>
            Página de Bienvenida
        </h2>
    </div>



    <div class="panel-body" style="padding:30px">
        <div class="textoBienvenida">Hola {{Auth::user()->name}}, has iniciado sesión como administrador del IES pramon.
            Si deseas cambiar tus datos de perfil como correo, foto o contraseña debes pulsar sobre tu nombre
            que aparece arriba a la derecha. recuerda que debes tener una dirección de correo  configurada para poder
            recuperar tu contraseña en caso de olvido.
        </div>
    </div>
</div>

  @stop
