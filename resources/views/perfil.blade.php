@extends('layouts.master')

@section('content')
@if ($errors->has('newMail') || $errors->has('repNewMail'))
<script>
    
    $(window).bind("load", function() {
    
    mostrarBloque('panelCambioEmail')
    
});
</script>
@endif


<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    Modificar Perfil de usuario
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">



                <form action="{{action('PerfilController@postPerfil')}}" enctype="multipart/form-data" method="POST">



                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}


                    <div class="row"><!--abrimos 2º row-->
                        <div class="col-sm-8"><!--columna de los textos-->
                            <div class="form-group">

                                <label for="nombre">Nombre:</label>
                                <label id="nombre">{{$user->name}}</label>

                            </div>
                            <div class="form-group">

                                <label for="email">E-Mail:</label>
                                <label id="email">{{$user->email}}</label>
                                <label  class="btn btn-info btn-xs" onclick="toggle_visibility('panelCambioEmail');">
                                    Cambiar
                                </label>


                            </div>

                            <div class="panel panel-default bloqueOculto" id="panelCambioEmail"><!--inicio panel Cambiar email-->
                                <div class="panel-heading">Cambiar Email (Si no rellena nada, conservará su email actual)</div>
                                <div class="panel-body">




                                    <div class="form-group">

                                        <label for="newMail">Nuevo E-mail:</label>
                                        <input type="text" name="newMail" id="newMail">
                                        <a href="#" class="glyphicon glyphicon-info-sign" data-toggle="popover" tabindex="0" data-trigger="focus" title="INFO" data-content="El Email será su nombre de usuario, deje este campo vacío para conservar su dirección de email actual"></a>
                                    </div>

                                    @if ($errors->has('newMail'))
                                    <div class="alert alert-danger">
                                        
                                        @foreach ($errors->get('newMail') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif

                                    <div class="form-group">

                                        <label for="repNewMail">Repita Nuevo Email:</label>
                                        <input type="text" name="repNewMail" id="repNewMail">
                                    </div>
                                    @if ($errors->has('repNewMail'))
                                    <div class="alert alert-danger">
                                        
                                        @foreach ($errors->get('repNewMail') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif


                                </div>
                            </div><!--Fin panel contraeña-->


                            <div class="form-group">

                                <label for="telefono">Teléfono:</label>
                                <input type="text" name="telefono" id="telefono" value="{{$user->telefono}}" size="9">
                            </div>
                            @if ($errors->has('telefono'))
                            <div class="alert alert-danger">

                                @foreach ($errors->get('telefono') as $message)
                                {{ $message }}<br>
                                @endforeach

                            </div>
                            @endif


                            <div class="panel panel-default"><!--inicio panel contraseña-->
                                <div class="panel-heading">Solo si desea cambiar su actual contraseña</div>
                                <div class="panel-body">




                                    <div class="form-group">

                                        <label for="newPwd">Nueva Contraseña:</label>
                                        <input type="password" name="newPwd" id="newPwd"   minlength="6">
                                        <a href="#" class="glyphicon glyphicon-info-sign" data-toggle="popover" tabindex="0" data-trigger="focus" title="INFO" data-content="La contraseña debe contener al menos 6 caracteres"></a>
                                    </div>

                                    @if ($errors->has('newPwd'))
                                    <div class="alert alert-danger">

                                        @foreach ($errors->get('newPwd') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif

                                    <div class="form-group">

                                        <label for="repNewPwd">Repita Contraseña:</label>
                                        <input type="password" name="repNewPwd" id="repNewPwd"   minlength="6">
                                    </div>
                                    @if ($errors->has('repNewPwd'))
                                    <div class="alert alert-danger">

                                        @foreach ($errors->get('repNewPwd') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif


                                </div>
                            </div><!--Fin panel contraeña-->



                            <div class="form-group">
                                <label for="foto">Foto:</label>
                               <!-- <span name="foto" id="foto" class="btn btn-default btn-file">-->
                                <input name="foto" id="foto" type="file" value="{{$user->foto}}">          
                            </div>
                            @if ($errors->has('foto'))
                            <div class="alert alert-danger">

                                @foreach ($errors->get('foto') as $message)
                                {{ $message }}<br>
                                @endforeach

                            </div>
                            @endif
                        </div><!--cerramos columna de los textos-->
                        <div class="col-sm-4"><!--columna de la foto-->
                            <img src="{{$user['foto']}}" style="height:100px"/>



                        </div><!--cerramos columna de  la foto-->

                    </div> <!--cerramos 2º row-->

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-info" style="padding:8px 100px;margin-top:25px;">
                            Modificar perfil
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop
