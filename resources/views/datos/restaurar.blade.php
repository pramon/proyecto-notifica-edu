@extends('layouts.master')

@section('content')
  
<script>

    function confirmarRestauracion()
    {
        var enviar = document.getElementById("idCopiaSeleccionada");
        var idEnviar=enviar.value = get_radio_value();
        // alert('No has seleccionado ninguna copia ');
        if (enviar.value == -2) {
            alert('No has seleccionado ninguna copia ');
            return false;
        }
        var nombreElemento="nombreCopia".concat(idEnviar);
        var nombreCopia = document.getElementById(nombreElemento);
        var nombreStr=nombreCopia.innerHTML.toString()
        
        var x = confirm("Los datos que tiene actualmente en la base de datos se perderán ¿Desea restaurar la copia de seguridad"+nombreStr+"?");
        if (x) {
            return true;
        } else
            return false;
    }

    function get_radio_value() {
        var inputs = document.getElementsByName("radio");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) {
                return inputs[i].value;
            }
        }
        return -2;
    }
    


</script>

<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if (Session::has('flash-message'))
        <div class="alert alert-success">
            {{ Session::get('flash-message') }}
        </div>
        @endif
       
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
                    Restaurar copia de seguridad
                </h3>
            </div>
            <div class="alert alert-warning">
                AVISO: Al restaurar una copia de seguridad, los datos que tiene actualmente se PERDERAN, si no está seguro
                de la operación se aconseja realizar una copia de seguridad de los datos actuales ANTES de restaurar<br>

            </div>


            <div class="panel-body" style="padding:30px">



                <form action="{{url('/admin/restaurar')}}" enctype="multipart/form-data" method="POST" onsubmit="return confirmarRestauracion()">
                    <div class="form-group pull-left ">
                        <input type="text" class="search form-control" placeholder="Introduce búsqueda">
                    </div>
                    


                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}
                   <div>
                        <span class="counter pull-right"></span>
                        <table class="table table-hover table-bordered results">
                            <thead  class= "thead-inverse" >
                                <tr><th>#</th><th>Nombre</th><th>Fecha</th><th>Seleccionar</th></tr>
                                <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No hay resultados</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $arrayCopias as $key => $copia )
                                <tr><th scope="row">{{$key+1}}</th>
                                    <td >
                                        <div style="padding-top: 2px" id="nombreCopia{{$copia->id}}"> {{$copia->nombre}}</div>
                                    </td>
                                    <td>
                                        <div style="padding-top: 2px"> {{$copia->created_at}}</div>
                                    </td>
                                    <td>
                                        <input type="radio"  value="{{$copia->id}}" name="radio" class="radioInput">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" id="idCopiaSeleccionada" value="-2" name="idCopiaSeleccionada"/>
                    </div>

         
              


                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Restaurar copia 
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop
