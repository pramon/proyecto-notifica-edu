@extends('layouts.master')

@section('content')


<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>
                    Copia de seguridad del Sistema
                </h3>
            </div>
            <div class="alert alert-warning">
                AVISO: Esta operación puede tardar unos minutos

            </div>


            <div class="panel-body" style="padding:30px">



                <form action="{{url('/admin/copia')}}" enctype="multipart/form-data" method="POST">



                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label for="nombre">Nombre de la copia</label>
                       
                        <input name="nombre" id="nombre" type="text" >
                         <a href="#" class="glyphicon glyphicon-info-sign" data-toggle="popover" tabindex="0" data-trigger="focus" title="INFO" data-content="El sistema guardará el nombre de la copia junto con la fecha exacta de su creación, aun así procure poner un nombre representativo"></a>
                    </div>
                    @if ($errors->has('nombre'))
                    <div class="alert alert-danger">

                        @foreach ($errors->get('nombre') as $message)
                        {{ $message }}<br>
                        @endforeach

                    </div>
                    @endif


                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Crear copia de seguridad
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop

