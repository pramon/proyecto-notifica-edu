@extends('layouts.master')

@section('content')
<script>

  function confirmarImportacion()
  {
  var x = confirm("¿Está seguro de que desea importar los datos?");
  if (x)
    return true;
  else
    return false;
  }

</script>

<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                    Importar Datos desde archivo
                </h3>
            </div>
            <div class="alert alert-warning">
                AVISO: al importar desde el archivo, los datos actuales, serán almacenados en una copia de seguridad<br>
                los datos que hay ahora en el sistema ya no serán accesibes. Esta operación solo es aconsejable hacerla <br>
                a principio de curso

            </div>


            <div class="panel-body" style="padding:30px">



                <form action="{{url('/admin/importar')}}" enctype="multipart/form-data" method="POST" onsubmit="return confirmarImportacion()">



                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label for="archivo">Archivo</label>
                       <!-- <span name="foto" id="foto" class="btn btn-default btn-file">-->
                        <input name="archivo" id="archivo" type="file" >          
                    </div>
                    @if ($errors->has('archivo'))
                    <div class="alert alert-danger">

                        @foreach ($errors->get('archivo') as $message)
                        {{ $message }}<br>
                        @endforeach

                    </div>
                    @endif


                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Crear copia e importar
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop
