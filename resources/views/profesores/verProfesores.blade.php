@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                    Ver Profesores
                </h3>
            </div>



            <div class="panel-body" style="padding:30px">
                <div class="form-group pull-left ">
                    <input type="text" class="search form-control" placeholder="Introduce búsqueda">
                </div>

                <div>
                    <span class="counter pull-right"></span>
                    <table class="table table-hover table-bordered results">
                        <thead  class= "thead-inverse" ><tr><th>#</th><th>Nombre </th><th>Rol</th><th>E-mail</th></tr>
                            <tr class="warning no-result">
                                <td colspan="4"><i class="fa fa-warning"></i> No hay resultados</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $arrayUsuarios as $key => $usuario )
                            <tr><th scope="row">{{$key+1}}</th>
                                <td>
                                    {{$usuario->name}}
                                </td>
                                <td>
                                    @if($usuario->rol==2)
                                    Administador
                                    @endif
                                    @if($usuario->rol==3)
                                    Profesor
                                    @endif
                                </td>
                                <td>
                                    {{$usuario->email}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>


</div>

@stop
