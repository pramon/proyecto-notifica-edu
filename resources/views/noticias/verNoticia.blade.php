@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Vista expandida de la noticia
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">
                 <a href="{{URL::previous() }}">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        Volver
                 </a>
                <div class="fotoNoticia"><img src="{{url('/').'/'.$noticia['fotoExp']}}"/></div>
                <div class="tituloNoticia"><h3> {{$noticia->titulo}}</h3></div>
                <div style="text-align: right; color: #999">{{\Carbon\Carbon::parse($noticia->orden)->formatLocalized('%d %B %Y')}}</div>
                <div class="cuerpoNoticia"> {{$noticia->cuerpo}}</div>
                
                
              

            </div>
        </div>
    </div>


</div>

@stop


