@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Editar Noticia
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">
                 <a href="{{URL::previous() }}">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        Volver
                 </a>



                <form action="{{action('NoticiaController@guardarNoticia',$noticia->id)}}" enctype="multipart/form-data" method="POST">


                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}
                    
                    <div class="form-group">

                        <!--<div class="fotoNoticia"><img src="../../../{{$noticia['fotoExp']}}"/></div>-->
                        <div class="fotoNoticia"><img src="{{url('/').'/'.$noticia['fotoExp']}}"/>
                        
                        <div class="form-group">
                            <label for="foto"> Cambiar Foto:</label>
                           
                            <input name="foto" id="foto" type="file">          
                        </div>
                        @if ($errors->has('foto'))
                        <div class="alert alert-danger">

                            @foreach ($errors->get('foto') as $message)
                            {{ $message }}<br>
                            @endforeach

                        </div>
                        @endif
                        
                        <h3><label for="titulo">Título:</label></h3>
                        <input type="text" name="titulo" id="titulo" class="form-control input-lg" value="{{$noticia->titulo}}">
                        @if ($errors->has('titulo'))
                        <div class="alert alert-danger">

                            @foreach ($errors->get('titulo') as $message)
                            {{ $message }}<br>
                            @endforeach

                        </div>
                        @endif

                    </div>
                    <!-- <div class="form-group">-->

                    <label for="cuerpo">Cuerpo:</label><br>
                    <textarea type="text" name="cuerpo" id="cuerpo">{{$noticia->cuerpo}}</textarea>
                    @if ($errors->has('cuerpo'))
                    <div class="alert alert-danger">

                        @foreach ($errors->get('cuerpo') as $message)
                        {{ $message }}<br>
                        @endforeach

                    </div>
                    @endif



                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">
                            Modificar
                        </button>
                        <a href="{{URL::previous() }}" class="btn btn-danger" role="button">Cancelar</a>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop


