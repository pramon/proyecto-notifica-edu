@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}

        </div>
        @endif
        
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Noticias Publicadas
                </h3>
            </div>
            <div class="alert alert-warning">
                Cuando un padre inicie la aplicación móvil esta será la lista de noticias que verá en su dispositivo, al pulsar sobre una de ellas aparecerá la noticia en detalle.
                Si desea editar una noticia o cambiar el orden en el que se muestran, deberá hacerlo en el apartado Todas las Noticias
            </div>


            <div class="panel-body" style="padding:5px">


                {{-- TODO: Protección contra CSRF --}}
                {{ csrf_field() }}
                
                <div>
                    <span class="counter pull-right"></span>
                    <table class="table table-bordered tablaNoticia">
                        <thead  class= "thead-inverse" >
                            <tr><th>Foto</th><th>Noticia</th></tr>

                        </thead>
                        <tbody>
                            @foreach( $arrayNoticias as $key => $noticia )

                            <tr class="clickable-row " data-href="{{ url('/admin/consultar/noticia/' . $noticia->id ) }}"><td class="vert-align-foto" rowspan="2"><img src="{{url('/').'/'.$noticia->foto}}" /></td><td><div style=" font-size: large; font-weight: bold"> {{$noticia->titulo}}</div></td></tr>
                            <tr class="clickable-row" data-href="{{ url('/admin/consultar/noticia/' . $noticia->id ) }}" ><td><div style="text-align: right; color: #999">{{\Carbon\Carbon::parse($noticia->orden)->formatLocalized('%d %B %Y')}}</div>{{$noticia->cuerpoRed}}</td></tr>

                            @endforeach
                        </tbody>
                    </table>

                </div>



            </div>
        </div>
    </div>


</div>

@stop
