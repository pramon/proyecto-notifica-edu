@extends('layouts.master')

@section('content')
<script>
    function confirmarEliminar(key)
    {  
        //alert('no está bien');
    /*    if(numNot===undefined){
            alert('no está bien');
            return false;
        }*/
        var idElemento="tituloNoticia";
        idElemento=idElemento.concat(key);
        var titulo = document.getElementById(idElemento);
        var textoConfirmacion="¿Desea eliminar la noticia ";
        var tituloTexto=titulo.innerHTML.toString();
        textoConfirmacion=textoConfirmacion+tituloTexto+"?"
       // textoConfirmacion.concat("?")
       
        var x = confirm(textoConfirmacion);
       
        if (x) {
            return true;
        } else
            return false;
    }
</script>



<div class="row">

    <div class="col-md-12">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}

        </div>
        @endif

        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Todas las noticias
                </h3>
            </div>
            <div class="alert alert-warning">
                Desde aquí se puedrán <strong>ver, editar, publicar ocultar o eliminar </strong> todas las noticias del sistema, en la vista noticias publicadas
                podrá ver las noticias que lo padres verán en su dispositivo. Pulsa sobre la foto para ver la vista expandida de la noticia
            </div>


            <div class="panel-body" style="padding:5px">
               


                {{-- TODO: Protección contra CSRF --}}
                {{ csrf_field() }}

                <div>

                    <table class="table table-bordered tablaNoticia">
                        <thead  class= "thead-inverse" >
                            <tr><th>Foto</th><th>Noticia</th><th>Publicada</th><th>Acciones</th></tr>

                        </thead>
                        <tbody>
                            @foreach( $arrayNoticias as $key => $noticia )

                            <tr>
                                <td class="vert-align-foto" rowspan="2">
                                    <a href="{{url('/admin/consultar/noticia/'.$noticia->id)}}">  <img src="{{url('/').'/'.$noticia->foto}}" /></a>
                                </td>
                                <td>
                                    <div id="tituloNoticia{{$key}}" style=" font-size: large; font-weight: bold"> {{$noticia->titulo}}</div>
                                </td>
                                <td rowspan="2" class="vert-align-foto" > 
                                    
                                    @if($noticia->mostrar)
                                    <span class="label label-success">SI</span>
                                    @else
                                    <span class="label label-danger">NO</span>
                                    @endif
                                </td>
                                <td rowspan="2" style="text-align: center">
                                    
                                    
                                        <!--Botones de edicion --->
                                        @if($noticia->mostrar)
                                       
                                            <form action="{{action('NoticiaController@putOcultar', $noticia->id)}}" 
                                                  method="POST" style="display:inline">
                                                {{ method_field('PUT') }}
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-warning btn-sm" style="display:inline;width: 60px">
                                                    Ocultar
                                                </button>
                                            </form>
                                        
                                        @else
                                        
                                            <form action="{{action('NoticiaController@putPublicar', $noticia->id)}}" 
                                                  method="POST" style="display:inline">
                                                {{ method_field('PUT') }}
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-success  btn-sm" style="display:inline;width: 60px">
                                                    Publicar
                                                </button>
                                            </form>
                                        
                                        @endif
                                         <div style="padding-top: 10px" >
                                            <form action="{{action('NoticiaController@deleteNoticia', $noticia->id)}}" 
                                                  method="POST" style="display:inline;" onsubmit="return confirmarEliminar({{$key}})">
                                                {{ method_field('DELETE') }}
                                                {!! csrf_field() !!}
                                                <button type="submit" class=" btn btn-danger btn-sm colorClaroWarning" style="width: 60px">
                                                    Eliminar
                                                </button>
                                            </form>
                                        
                                         </div>
                                        
                                        
                                        <div style="padding-top: 10px">
                                            <form action="{{action('NoticiaController@editNoticia', $noticia->id)}}" 
                                                  method="POST" style="display:inline;">
                                                {{ method_field('PUT') }}
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-primary btn-sm" style="display:inline;width: 60px;">
                                                    Editar
                                                </button>
                                            </form>
                                        </div>
                                        
                                        
                                        @if($noticia->mostrar)
                                         <div style="padding-top: 10px">
                                            <form action="{{action('NoticiaController@putSubir', $noticia->id)}}" 
                                                  method="POST" style="display:inline">
                                                {{ method_field('PUT') }}
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-info btn-sm" style="display:inline;width: 60px">
                                                    Subir 
                                                </button>
                                            </form>
                                         </div>
                                        
                                        @endif
                                        

                                    

                                </td>
                            </tr>
                            <tr ><td><div style="text-align: right; color: #999">{{\Carbon\Carbon::parse($noticia->orden)->formatLocalized('%d %B %Y')}}</div>{{$noticia->cuerpoRed}}</td></tr>

                            @endforeach
                        </tbody>
                    </table>

                </div>



            </div>
        </div>
    </div>


</div>

@stop
