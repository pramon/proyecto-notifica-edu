@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Nueva Noticia
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">



                <form action="{{action('NoticiaController@postNoticia')}}" enctype="multipart/form-data" method="POST">


                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}
                    <div class="alert alert-warning">
                        En este formulario crearás la noticia y a continuación podrás ver una vista previa y publicarla si así lo desas
                    </div>



                    <div class="form-group">

                        <h3><label for="titulo">Título:</label></h3>
                        <input type="text" name="titulo" id="titulo" class="form-control input-lg">
                        @if ($errors->has('titulo'))
                        <div class="alert alert-danger">

                            @foreach ($errors->get('titulo') as $message)
                            {{ $message }}<br>
                            @endforeach

                        </div>
                        @endif

                    </div>
                   <!-- <div class="form-group">-->

                   <label for="cuerpo">Cuerpo:</label><br>
                        <textarea type="text" name="cuerpo" id="cuerpo"></textarea>
                        @if ($errors->has('cuerpo'))
                        <div class="alert alert-danger">

                            @foreach ($errors->get('cuerpo') as $message)
                            {{ $message }}<br>
                            @endforeach

                        </div>
                        @endif

                  <!--  </div>-->


               



                    <div class="form-group">
                        <label for="foto">Foto:(Opcional)</label>
                       <!-- <span name="foto" id="foto" class="btn btn-default btn-file">-->
                        <input name="foto" id="foto" type="file">          
                    </div>
                    @if ($errors->has('foto'))
                    <div class="alert alert-danger">

                        @foreach ($errors->get('foto') as $message)
                        {{ $message }}<br>
                        @endforeach

                    </div>
                    @endif




                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-info" style="padding:8px 100px;margin-top:25px;">
                            Crear Noticia 
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop


