@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        
        
       

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                    Publicar Noticia
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">
                <div class="alert alert-success">
                    Vista previa de la noticia, Debe CONFIRMAR su publicación
                </div>
                <div class="fotoNoticia"><img src="{{url('/').'/'.$noticia['fotoExp']}}"/></div>
                <div class="tituloNoticia"><h3> {{$noticia->titulo}}</h3></div>
                
                <div class="cuerpoNoticia"> {{$noticia->cuerpo}}</div>
                
                
                <!--botones-->
                <div class="row">
                    <div class="col-md-3">
                        <form action="{{action('NoticiaController@putPublicar', $noticia->id)}}" 
                              method="POST" style="display:inline">
                            {{ method_field('PUT') }}
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-success" style="display:inline; width:100px">
                                Publicar
                            </button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form action="{{action('NoticiaController@putGuardarSinPub', $noticia->id)}}" 
                              method="POST" style="display:inline">
                            {{ method_field('PUT') }}
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-warning" style="display:inline; width:100px">
                                Solo guardar
                            </button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form action="{{action('NoticiaController@editNoticia', $noticia->id)}}" 
                              method="POST" style="display:inline">
                            {{ method_field('PUT') }}
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-primary" style="display:inline; width:100px">
                                Editar
                            </button>
                        </form>
                    </div>
                     <div class="col-md-3">
                        <form action="{{action('NoticiaController@deleteNoticia', $noticia->id)}}" 
                              method="POST" style="display:inline">
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger" style="display:inline; width:100px">
                                Eliminar
                            </button>
                        </form>
                    </div>

                </div>





            </div>
        </div>
    </div>


</div>

@stop


