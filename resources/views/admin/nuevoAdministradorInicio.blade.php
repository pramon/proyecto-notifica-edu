@extends('layouts.master')

@section('content')



<div class="row">

    <div class="col-md-offset-2 col-md-8">
        @if(session('mensaje'))
        <div class="alert alert-success">

            {{ session('mensaje') }}
        </div>
        @endif
        @if(session('mensajeError'))
        <div class="alert alert-danger">

            {{ session('mensajeError') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    Crear administrador Inicio
                </h3>
            </div>

            <div class="panel-body" style="padding:30px">



                <form action="{{action('AdminController@postAdministrador')}}" enctype="multipart/form-data" method="POST">



                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}


                   
                            <div class="form-group">

                                <label for="nombre">Nombre:</label>
                                <input type="text" name="nombre" id="nombre">
                                @if ($errors->has('nombre'))
                            <div class="alert alert-danger">

                                @foreach ($errors->get('nombre') as $message)
                                {{ $message }}<br>
                                @endforeach

                            </div>
                            @endif

                            </div>
                            

                            <div class="panel panel-default" id="panelCambioEmail"><!--inicio panel  email-->
                                <div class="panel-heading">Email</div>
                                <div class="panel-body">




                                    <div class="form-group">

                                        <label for="newMail">Nuevo E-mail:</label>
                                        <input type="text" name="newMail" id="newMail">
                                        <a href="#" class="glyphicon glyphicon-info-sign" data-toggle="popover" tabindex="0" data-trigger="focus" title="INFO" data-content="El Email será su nombre de usuario, es importante que coincida con el email que tiene como profesor para que se vinculen correctamente las dos cuentas"></a>
                                    </div>

                                    @if ($errors->has('newMail'))
                                    <div class="alert alert-danger">
                                        
                                        @foreach ($errors->get('newMail') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif

                                    <div class="form-group">

                                        <label for="repNewMail">Repita Nuevo Email:</label>
                                        <input type="text" name="repNewMail" id="repNewMail">
                                    </div>
                                    @if ($errors->has('repNewMail'))
                                    <div class="alert alert-danger">
                                        
                                        @foreach ($errors->get('repNewMail') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif


                                </div>
                            </div><!--Fin panel contraeña-->


                            <div class="form-group">

                                <label for="telefono">Teléfono:</label>
                                <input type="text" name="telefono" id="telefono"  size="9">
                            </div>
                            @if ($errors->has('telefono'))
                            <div class="alert alert-danger">

                                @foreach ($errors->get('telefono') as $message)
                                {{ $message }}<br>
                                @endforeach

                            </div>
                            @endif


                            <div class="panel panel-default"><!--inicio panel contraseña-->
                                <div class="panel-heading">Contraseña del Usuario, después deberá cambiarla, si lo desea puede inventarse una que después el usuario la restablezca usando la restauaración (Ha olvidado su contraseña?) </div>
                                <div class="panel-body">




                                    <div class="form-group">

                                        <label for="newPwd">Contraseña:</label>
                                        <input type="password" name="newPwd" id="newPwd"   minlength="6">
                                        <a href="#" class="glyphicon glyphicon-info-sign" data-toggle="popover" tabindex="0" data-trigger="focus" title="INFO" data-content="La contraseña debe contener al menos 6 caracteres"></a>
                                    </div>

                                    @if ($errors->has('newPwd'))
                                    <div class="alert alert-danger">

                                        @foreach ($errors->get('newPwd') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif

                                    <div class="form-group">

                                        <label for="repNewPwd">Repita Contraseña:</label>
                                        <input type="password" name="repNewPwd" id="repNewPwd"   minlength="6">
                                    </div>
                                    @if ($errors->has('repNewPwd'))
                                    <div class="alert alert-danger">

                                        @foreach ($errors->get('repNewPwd') as $message)
                                        {{ $message }}<br>
                                        @endforeach

                                    </div>
                                    @endif


                                </div>
                            </div><!--Fin panel contraeña-->



                            <div class="form-group">
                                <label for="foto">Foto:(Opcional)</label>
                               
                                <input name="foto" id="foto" type="file">          
                            </div>
                            @if ($errors->has('foto'))
                            <div class="alert alert-danger">

                                @foreach ($errors->get('foto') as $message)
                                {{ $message }}<br>
                                @endforeach

                            </div>
                            @endif
                       
                       
                   

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-info" style="padding:8px 100px;margin-top:25px;">
                           Crear Administrador
                        </button>
                    </div>


                </form>

            </div>
        </div>
    </div>


</div>

@stop

