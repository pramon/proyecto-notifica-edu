<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */







/* El siguiente metodo generara automaticamente las siguientes rutas:
  public function auth()
  {
  // Authentication Routes...
  $this->get('login', 'Auth\AuthController@showLoginForm');
  $this->post('login', 'Auth\AuthController@login');
  $this->get('logout', 'Auth\AuthController@logout');
  // Registration Routes...
  $this->get('register', 'Auth\AuthController@showRegistrationForm');
  $this->post('register', 'Auth\AuthController@register');
  // Password Reset Routes...
  $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
  $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
  $this->post('password/reset', 'Auth\PasswordController@reset');
  }
 */
//la unica forma humana de que coga la hora de aqui
date_default_timezone_set('Europe/Madrid');
//para que los farmatos de fecha esten en español
setlocale(LC_TIME, 'es_ES.UTF-8');
Route::auth();



//En la version 5.2 ya no hau que poner middlewhare 'web'

Route::get('/', 'HomeController@index');

//API para los noticias no necesitamos estar autenticados
Route::get('/API/getNoticias', 'NoticiaController@getNoticiasAPI');


//Solo  bajo el control de usuarios autenticados
Route::group(['middleware' => 'auth'], function() {


    Route::get('/admin/bienvenida','HomeController@index');
    
    
    Route::get('/perfil', 'PerfilController@getPerfil');
    Route::post('/modificarPerfil', 'PerfilController@postPerfil');
    
    
    //Solo bajo el control de administradores
    
    
    //Rutas de datos, importar, exportar, copia
    Route::get('/admin/datos/importar', function () {
        return view('datos/importar');
    });
    Route::post('/admin/importar', 'AdminController@postImportar');
    
    Route::get('/admin/datos/restaurar','AdminController@getRestaurar');
    Route::post('/admin/restaurar', 'AdminController@postRestauracion');
    Route::post('/admin/copia', 'AdminController@postCopia');
    Route::get('/admin/datos/exportar', function () {
        return view('datos/exportar');
    });
    Route::get('/admin/datos/importar', function () {
        return view('datos/importar');
    });
    Route::get('/admin/datos/generar', function () {
        return view('datos/generar');
    });
    
    
    //Rutas de noticias
    Route::post('/admin/noticia/crear', 'NoticiaController@postNoticia');
    Route::put('/admin/noticia/publicar/{id}', 'NoticiaController@putPublicar');
    Route::put('/admin/noticia/ocultar/{id}', 'NoticiaController@putOcultar');
    //cargar noticia en el formulario de edicion
    Route::put('/admin/noticia/modificar/{id}', 'NoticiaController@editNoticia');
    //Guaradr noticia modificada
    Route::post('/admin/noticia/guardar/{id}', 'NoticiaController@guardarNoticia');
    
    Route::put('admin/noticia/guardarSinPub/{id}','NoticiaController@putGuardarSinPub');
    Route::put('/admin/noticia/subir/{id}', 'NoticiaController@putSubir');
    Route::delete('/admin/noticia/eliminar/{id}', 'NoticiaController@deleteNoticia');
    Route::get('/admin/consultar/noticias','NoticiaController@getNoticiasPublicadas');
    Route::get('/admin/consultar/todasNoticias','NoticiaController@getTodasNoticias');
    Route::get('/admin/consultar/noticia/{id}', 'NoticiaController@getNoticia');
    Route::get('/admin/insertar/noticia', function () {
        return view('noticias/nuevaNoticia');
    });
    
    
    
   
    
    Route::get('/admin/consultar/alumnos', function () {
        return view('alumnos/verAlumnos');
    });
    Route::get('/admin/consultar/usuarios','UserController@getUsuarios');
    Route::get('/admin/consultar/grupos', function () {
        return view('grupos/verGrupos');
    });
    Route::get('/admin/consultar/familias', function () {
        return view('familias/verFamilias');
    });
    Route::get('/admin/consultar/notificaciones', function () {
        return view('notificaciones/verNotificaciones');
    });
    
    
    
    Route::get('/admin/insertar/alumno', function () {
        return view('alumnos/nuevoAlumno');
    });
    Route::get('/admin/insertar/familia', function () {
        return view('familias/nuevaFamilia');
    });
    Route::get('/admin/insertar/notificacionAlumno', function () {
        return view('notificaciones/nuevaNotAlumno');
    });
    Route::get('/admin/insertar/notificacionGlobal', function () {
        return view('notificaciones/nuevaNotGlobal');
    });
    
    
    Route::get('/admin/asignar/famAlu', function () {
        return view('alumnos/asignarFamilia');
    });
    Route::get('/admin/asignar/grupAlu', function () {
        return view('alumnos/asignarGrupo');
    });
    Route::get('/admin/asignar/rolAdmin', function () {
        return view('admin/asignarRolAdmin');
    });
    
    
    
    //Solo bajo el control de superadmin
    Route::get('/superadmin/crear/administrador', function () {
        return view('admin/nuevoAdministradorInicio');
    });
     Route::post('/crearAdministrador', 'AdminController@postAdministrador');
    
    
});

