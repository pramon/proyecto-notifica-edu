<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //'auth' es un middleware gloabal definido en Kernel.php
        //La clase que lo implementa es \App\Http\Middleware\Authenticate::class,
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        //aqui debemos cargar 
        
        //aqui ya estamos autenticados
        if (Auth::user()->rol < 3) {
            return view('welcomeAdmin');
        } else {
            return view('welcomeProfesor');
        }
    }

}
