<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller {

    public function getUsuarios(Request $request) {
        
        $usuarios = DB::table('users')->where('rol', '>', 1)->get();
        return view('profesores.verProfesores', array('arrayUsuarios' =>$usuarios));
        /*foreach ($files as $file) {
            //echo (string) $file, "\n";
            
        }*/
    }
    

}
