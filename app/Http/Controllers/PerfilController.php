<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use Intervention\Image\ImageManagerStatic as Image;

class PerfilController extends Controller {

    public function getPerfil() {
        $user = Auth::user();

        return view('perfil')->with(['user' => $user]);
    }

    public function postPerfil(Request $request) {
        $messages = [
            'newPwd.min' => 'La contraseña debe tener al menos 6 caracteres',
            'repNewPwd.same' => 'Las contraseñas no coinciden',
            'repNewPwd.required_with' => 'Las contraseñas no coinciden',
            'newMail.email' => 'No es una dirección de email válida',
            'repNewMail.same' => 'Los e-mail no coinciden',
            'repNewMail.required_with' => 'Las email no coinciden',
            'telefono.required' => 'Debe indicar un teléfono',
            'telefono.min' => 'El telefono debe constar de 9 cifras',
            'foto.image' => 'El archivo debe ser una foto',
            'foto.mimes' => 'La foto debe tener uno de estos formatos:jpg,png,jpeg'
            
        ];
        $validator = Validator::make($request->all(), [
                    'newPwd' => 'min:6',
                    'repNewPwd' => 'same:newPwd|required_with:newPwd',
                     'newMail' => 'email',
                    'repNewMail' => 'same:newMail|required_with:newMail',
            
            
                    'telefono'=>'required|min:9',
                    'foto' => 'image|mimes:jpg,png,jpeg'
                        ], $messages);

       
       
        
        if ($validator->fails()) {
            return redirect('/perfil')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $user = Auth::user();
            $pwd = $request->input('newPwd');
            $tel=$request->input('telefono');
            $email=$request->input('newMail');
            
            if(!empty($email)){
                $userRep = DB::table('users')->where('email',$email)->first();
                if(!empty($userRep)){
                    
                    return redirect('/perfil')->with('mensajeError', 'Ya existe un usuario con ese email');
                    
                }else{
                   // return redirect('/perfil')->with('mensajeError', 'Aqui en teoria el usuario no existe');
                   $user->email=$email;
                }
                
            }
            
            //guardan¡mos el passsword
            if($pwd){
                //lo encriptamos en el get
               $user->password = bcrypt($pwd);
            }
            $user->telefono=$tel;
          
            
            //Ahora introducimos la foto
            if ($request->file('foto')) {

                $image = $request->file('foto');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('imgPerfil/' . $filename);


                Image::make($image->getRealPath())->resize(100, 100)->save($path);
                $user->foto = 'imgPerfil/' . $filename;
                
            }
            $user->save();


            return redirect('/perfil')->with('mensaje', 'Registro Actualizado:');
        }
    }

}
