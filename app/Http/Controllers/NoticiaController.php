<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Noticia;
use DateTime;
use Flash;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class NoticiaController extends Controller {

    public function getTodasNoticias(Request $request) {

        $noticias = DB::table('noticias')->orderBy('mostrar','desc')->orderBy('orden', 'desc')->get();


        return view('noticias.verTodasNoticias', array('arrayNoticias' => $noticias));
    }

    public function getNoticiasPublicadas(Request $request) {

        $noticias = DB::table('noticias')->where('mostrar', 1)->orderBy('orden', 'desc')->get();

        return view('noticias.verNoticiasPublicadas', array('arrayNoticias' => $noticias));
    }

    public function getNoticia($id) {

        $noticia = Noticia::findOrFail($id);

        return view('noticias.verNoticia', array('noticia' => $noticia));
    }

    public function postNoticia(Request $request) {
        $messages = [
            'titulo.required' => 'La noticia debe tener un titular',
            'cuerpo.required' => 'Debes introducir contenido en la noticia',
            'foto.image' => 'El archivo debe ser una foto',
            'foto.mimes' => 'La foto debe tener uno de estos formatos:jpg,png,jpeg'
        ];
        $validator = Validator::make($request->all(), [
                    'titulo' => 'required',
                    'cuerpo' => 'required',
                    'foto' => 'image|mimes:jpg,png,jpeg'
                        ], $messages);




        if ($validator->fails()) {
            return redirect('/admin/insertar/noticia')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $noticia = new Noticia();
            $titulo = $request->input('titulo');
            $cuerpo = $request->input('cuerpo');

            $editor = Auth::user()->id;
            //el orden se establece con la fecha cuanto mas reciente mas arriba
            //posteriormente se puede cambiar para poner una noticia más arriba
            $orden = new DateTime();
            $mostrar = false;



            $noticia->titulo = $titulo;
            $noticia->cuerpo = $cuerpo;
            $noticia->editor = $editor;
            $noticia->orden = $orden;
            $noticia->mostrar = $mostrar;
            if (strlen($cuerpo) < 150) {
                $cuerpoRed = $cuerpo;
            } else {
                $cuerpoRed = substr($cuerpo, 0, 140) . '  ...(mas)';
            }
            $noticia->cuerpoRed = $cuerpoRed;


            //Ahora introducimos la foto
            if ($request->file('foto')) {

                $image = $request->file('foto');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('imgNoticias/' . $filename);
                $img = Image::make($image->getRealPath());
                
                $height = $img->height();
                $width = $img->width();
                
                if ($width >= $height) {
                    //mas ancha que larga
                    
                    $img->resize(120, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(null, 90, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                
                $img->save($path);
                $noticia->foto = 'imgNoticias/' . $filename;
                
                //ahora vamos a guardar la imagen expandida
                $pathExp = public_path('imgNoticiasExp/' . $filename);
                $imgExp = Image::make($image->getRealPath());
                
                $heightExp = $imgExp->height();
                $widthExp = $imgExp->width();
                
                if (($widthExp >= $heightExp) && $widthExp>300) {
                    //mas ancha que larga
                    
                    $imgExp->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } if (($heightExp > $widthExp) && $heightExp>300) {
                    $imgExp->resize(null, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                
                $imgExp->save($pathExp);
                $noticia->fotoExp = 'imgNoticiasExp/' . $filename;
                
                
                
               
            }else{
                //si el usuario no ha metido noticia ponemos la de por defecto
                
                  $noticia->foto = 'imgNoticias/imgDefectoLista.jpg';
                  $noticia->fotoExp = 'imgNoticiasExp/imgDefectoExp.jpg';
                
            }
            $noticia->save();
           
            return view('noticias.publicarNoticia', array('noticia' => $noticia));
        }
    }
    //mostar formulario de edicion con los datos
    public function editNoticia($id){
        $noticia = Noticia::findOrFail($id);
        return view('noticias.editarNoticia', array('noticia' => $noticia));
        
    }
    
    //Modificar la noticia
    public function guardarNoticia(Request $request,$id ) {
        $messages = [
            'titulo.required' => 'La noticia debe tener un titular',
            'cuerpo.required' => 'Debes introducir contenido en la noticia',
            'foto.image' => 'El archivo debe ser una foto',
            'foto.mimes' => 'La foto debe tener uno de estos formatos:jpg,png,jpeg'
        ];
        $validator = Validator::make($request->all(), [
                    'titulo' => 'required',
                    'cuerpo' => 'required',
                    'foto' => 'image|mimes:jpg,png,jpeg'
                        ], $messages);




        if ($validator->fails()) {
            return redirect('/admin/insertar/noticia')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $noticia = Noticia::findOrFail($id);
            $titulo = $request->input('titulo');
            $cuerpo = $request->input('cuerpo');

            $editor = Auth::user()->id;
            //el orden se establece con la fecha cuanto mas reciente mas arriba
            //posteriormente se puede cambiar para poner una noticia más arriba
            $orden = new DateTime();
            $mostrar = false;



            $noticia->titulo = $titulo;
            $noticia->cuerpo = $cuerpo;
            $noticia->editor = $editor;
            $noticia->orden = $orden;
            $noticia->mostrar = $mostrar;
            if (strlen($cuerpo) < 150) {
                $cuerpoRed = $cuerpo;
            } else {
                $cuerpoRed = substr($cuerpo, 0, 140) . '  ...(mas)';
            }
            $noticia->cuerpoRed = $cuerpoRed;


            //Ahora introducimos la foto
            if ($request->file('foto')) {

                $image = $request->file('foto');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('imgNoticias/' . $filename);
                $img = Image::make($image->getRealPath());
                
                $height = $img->height();
                $width = $img->width();
                
                if ($width >= $height) {
                    //mas ancha que larga
                    
                    $img->resize(120, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(null, 90, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                
                $img->save($path);
                $noticia->foto = 'imgNoticias/' . $filename;
                
                //ahora vamos a guardar la imagen expandida
                $pathExp = public_path('imgNoticiasExp/' . $filename);
                $imgExp = Image::make($image->getRealPath());
                
                $heightExp = $imgExp->height();
                $widthExp = $imgExp->width();
                
                if (($widthExp >= $heightExp) && $widthExp>300) {
                    //mas ancha que larga
                    
                    $imgExp->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } if (($heightExp > $widthExp) && $heightExp>300) {
                    $imgExp->resize(null, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                
                $imgExp->save($pathExp);
                $noticia->fotoExp = 'imgNoticiasExp/' . $filename;
                
                
                
               
            }
            //No hace falta poner la noticia por defecto se queda la que está
            $noticia->save();
           
            return view('noticias.publicarNoticia', array('noticia' => $noticia));
        }
    }
    
    public function putGuardarSinPub($id){
        //por defecto la noticia no está publicada por eso simplemento redireccionamos
        return redirect('/admin/consultar/todasNoticias')->with('mensaje','Noticia guardada pero no publicada');
        
    }

    public function putPublicar($id) {
        $n = Noticia::findOrFail($id);
        $n->mostrar = true;
        $n->save();
        
        return redirect('/admin/consultar/todasNoticias')->with('mensaje', 'Noticia Publicada');
    }
     public function putOcultar($id) {
        $n = Noticia::findOrFail($id);
        $n->mostrar = false;
        $n->save();
        
        return redirect('/admin/consultar/todasNoticias')->with('mensaje', 'Noticia Ocultada');;
    }
     public function deleteNoticia($id) {
        $n = Noticia::findOrFail($id);
        
        $n->delete();
        
        return redirect('/admin/consultar/todasNoticias')->with('mensaje', 'Noticia Eliminada Correctamente');;
    }
    public function putSubir($id) {
        //paasa una noticia al inicio de la lista
        $n = Noticia::findOrFail($id);
        $n->orden= new DateTime();
        $n->save();
        
        //TO DO
        return redirect('/admin/consultar/todasNoticias')->with('mensaje', 'La noticia se miestra ahora en primer lugar');;
    }
    
    //funciones para el API que serviran para conectarnos desde Android
    public function getNoticiasAPI(){
        $noticias = DB::table('noticias')->where('mostrar', 1)->orderBy('orden', 'desc')->get();
        return response()->json( $noticias );
    }
    


}
