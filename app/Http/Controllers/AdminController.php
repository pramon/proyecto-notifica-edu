<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DateTime;
use DateTimeZone;
use Carbon;
use Config;
use App\Copia;
use App\User;
use App\Noticia;
use Laracasts\Flash\Flash;
//use DateTime;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller {

    public function getRestaurar(Request $request) {
        
        $copias = DB::table('copias')->orderBy('created_at', 'desc')->get();
        return view('datos.restaurar', array('arrayCopias' => $copias));
       
    }

    public function postRestauracion(Request $request) {
        $seleccionado = $request->input('idCopiaSeleccionada');
        $copia = Copia::findOrFail($seleccionado);
        $contents = File::get($copia->path);
        $obj = json_decode($contents, true);

        //restauramos usuarios
        DB::table('users')->truncate();

        foreach ($obj['usuarios'] as $indice => $usuario) {
            $nuevo = new User();
            $nuevo->name = $usuario['name'];
            $nuevo->email = $usuario['email'];
            $nuevo->password = $usuario['password'];
            $nuevo->rol = $usuario['rol'];
            $nuevo->foto = $usuario['foto'];
            $nuevo->telefono = $usuario['telefono'];
            $nuevo->remember_token = $usuario['remember_token'];
            $nuevo->save();
        
        }
        //restauramos noticias
        DB::table('noticias')->truncate();

        foreach ($obj['noticias'] as $indice => $noticia) {
            $nuevo = new Noticia();
            $nuevo->titulo = $noticia['titulo'];
            $nuevo->cuerpo = $noticia['cuerpo'];
            $nuevo->cuerpoRed = $noticia['cuerpoRed'];
            $nuevo->foto = $noticia['foto'];
            $nuevo->fotoExp = $noticia['fotoExp'];
            $nuevo->editor = $noticia['editor'];
            $nuevo->orden = $noticia['orden'];
            $nuevo->mostrar = $noticia['mostrar'];
            
            $nuevo->save();
        
        }




        
        return  redirect()->back()->with('flash-message','Copia restaurada correctamente');  
       
    }

    public function postImportar(Request $request) {
        $messages = [

            'archivo.mimes' => 'El archivo de de ser formato .xml'
        ];
        $validator = Validator::make($request->all(), [
                    //cambiar
                    'archivo' => 'image|mimes:jpg,png,jpeg'
                        ], $messages);




        if ($validator->fails()) {
            return redirect('/admin/datos/importar')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            //to do copia de seguridad e importacion
            $this->crearJSON('AUTO');


            return redirect('/admin/datos/importar')->with('mensaje', 'La importación se ha realizado correctamente');
        }
    }
    public function postCopia(Request $request) {
        $messages = [

            'nombre.required' => 'Debes de poner un nombre a tu copia'
        ];
        $validator = Validator::make($request->all(), [
                    //cambiar
                    'nombre' => 'required'
                        ], $messages);




        if ($validator->fails()) {
            return redirect('/admin/datos/exportar')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            //to do copia de seguridad e importacion
            Config::set('app.timezone', 'Europe/Madrid');
            $nombre=$request->input('nombre');
            $this->crearJSON($nombre);

            
            return redirect('/admin/datos/exportar')->with('mensaje', 'La copia de seguridad se ha realizado correctamente');
        }
    }

    public function crearJSON($nombre) {
        try {
            $users = User::all(); //obtenemos los usuarios
            $usersJson = $users->toJson(); //esto ya es un string
           // $usersJson = '{"usuarios":' . $usersJson . '}';
            
            $noticias = Noticia::all(); //obtenemos las noticias
            $noticiasJson = $noticias->toJson(); //esto ya es un string
           // $noticiasJson = '{"noticias":' . $noticiasJson . '}';

            //falta resto de tablas

            $contenidoJSON = '{"usuarios":'.$usersJson.',"noticias":'.$noticiasJson.'}';
            
            $fecha=new DateTime();
          
            $fechaStr = $fecha->format('Y-m-d H:i:s');
            $filename = $nombre . 'copSeg-' . $fechaStr;
            $path = public_path('copiaSeg/' . $filename . '.json');
            File::put($path, $contenidoJSON);


            //Una vez terminada la copia
            $c = new Copia();
            $c->nombre = $nombre;
            $c->path = $path;
            $c->save();
        } catch (Exception $ex) {
            return redirect('/admin/datos/importar')->with('mensajeError', 'Ha habido un error al importar: ' . $ex);
        }
    }
    
    

    

    public function postAdministrador(Request $request) {
        $messages = [
            'nombre.required' => 'El nuevo usuario debe tener un nombre',
            'newPwd.min' => 'La contraseña debe tener al menos 6 caracteres',
            'newPwd.required' => 'Debes introducir una contraseña',
            'repNewPwd.same' => 'Las contraseñas no coinciden',
            'repNewPwd.required_with' => 'Las contraseñas no coinciden',
            'newMail.email' => 'No es una dirección de email válida',
            'newMail.required' => 'Es necesario introducir un email',
            'repNewMail.same' => 'Los e-mail no coinciden',
            'repNewMail.required_with' => 'Las email no coinciden',
            'telefono.required' => 'Debe indicar un teléfono',
            'telefono.min' => 'El telefono debe constar de 9 cifras',
            'foto.image' => 'El archivo debe ser una foto',
            'foto.mimes' => 'La foto debe tener uno de estos formatos:jpg,png,jpeg'
        ];
        $validator = Validator::make($request->all(), [
                    'newPwd' => 'min:6|required',
                    'nombre' => 'required',
                    'repNewPwd' => 'same:newPwd|required_with:newPwd',
                    'newMail' => 'email|required',
                    'repNewMail' => 'same:newMail|required_with:newMail',
                    'telefono' => 'required|min:9',
                    'foto' => 'image|mimes:jpg,png,jpeg'
                        ], $messages);




        if ($validator->fails()) {
            return redirect('/superadmin/crear/administrador')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $user = new User();
            $pwd = $request->input('newPwd');
            $tel = $request->input('telefono');
            $email = $request->input('newMail');
            $nombre = $request->input('nombre');


            $userRep = DB::table('users')->where('email', $email)->first();
            if (!empty($userRep)) {

                return redirect('/superadmin/crear/administrador')->with('mensajeError', 'Ya existe un usuario con ese email');
            }
            $user->name=$nombre;
            $user->email = $email;




            //guardan¡mos el passsword
            //lo encriptamos en el get
            $user->password = bcrypt($pwd);

            $user->telefono = $tel;
            $user->rol=2;


            //Ahora introducimos la foto
            if ($request->file('foto')) {

                $image = $request->file('foto');
                $filename = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('imgPerfil/' . $filename);


                Image::make($image->getRealPath())->resize(100, 100)->save($path);
                $user->foto = 'imgPerfil/' . $filename;
            }
            $user->save();


            return redirect('/superadmin/crear/administrador')->with('mensaje', 'El administrador se ha creado correctamente');
        }
    }

}
