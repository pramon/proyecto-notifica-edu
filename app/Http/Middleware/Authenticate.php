<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //guard es el usuario actual, el método determina si es invitado
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                //Si es una peticion a un servicio devuelve pagina Unauthorized
                return response('Unauthorized.', 401);
            } else {
                // Si no lo lleva a la paágina de login
                return redirect()->guest('login');
            }
        }
        //Si no lo es simplemente deja pasar la peticion
        return $next($request);
    }
}
