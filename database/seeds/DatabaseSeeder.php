<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->delete();
        $u= new User;
        $u->name='Pablo Ramón Manresa';
        $u->email='pramonprof@gmail.com';
        $u->password=bcrypt('111111');
        $u->rol=1;
        $u->telefono='965311465';
        $u->save();
        
        $u2= new User;
        $u2->name='paco';
        $u2->email='paco@pp.es';
        $u2->password=bcrypt('222222');
        $u->rol=2;
        $u->telefono='965311463';
        $u2->save();
    }
}
